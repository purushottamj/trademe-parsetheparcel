# README #

Instructions to use the solution

### Parse the Parcel ###

* Test Parse the Parcel Service to get Parcel Type and Cost
* Version 1.0

### Files/Folder information ###

* packagecostchecker, parsetheparces and parsetheparcel.sln together to be used in Visual Studio
* parsetheparcel is service which calls procedure to parse the cost
* packagecostcheck is console application to test the service
* script.sql is database script. Database name testing

### Test the solution ###

* Open the solutions in Visual Studio 2015
* Database Server - Localhost and Database Name testing
* Create Database with the same name and Execute the script.sql file 
* Run the project in Visual Studio (Press F5) and get result (Modify the values in Program.cs to get diff results)
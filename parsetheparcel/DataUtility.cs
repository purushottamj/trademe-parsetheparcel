﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace parsetheparcel
{
    class DataUtility : IDisposable
    {
        #region objects
        SqlConnection MyCon;
        SqlCommand cmd = new SqlCommand();
        #endregion

        #region Public Method
        public void OpenCon()
        {
            if (MyCon == null)
                MyCon = new SqlConnection(ConfigurationManager.ConnectionStrings["parsetheparcel"].ConnectionString);
            if (MyCon.State == ConnectionState.Closed)
                MyCon.Open();
            cmd.Connection = MyCon;
        }

        public void CloseCon()
        {
            if (MyCon.State == ConnectionState.Open)
                MyCon.Close();
        }

        public void Dispose()
        {
            if (MyCon != null)
            {
                MyCon.Dispose();
                MyCon = null;
            }
        }

        public void AddParameter(string paramname, object paramvalue)
        {
            SqlParameter param = new SqlParameter(paramname, paramvalue);
            cmd.Parameters.Add(param);
        }

        public void AddOutputParameter(string paramname, string type)
        {
            SqlParameter param = null;
            switch (type)
            {
                case "texttype":
                    param = new SqlParameter(paramname, SqlDbType.VarChar, 4000);
                    break;
                case "inttype":
                    param = new SqlParameter(paramname, SqlDbType.Int);
                    break;
            }
            cmd.Parameters.Add(param);
            param.Direction = ParameterDirection.Output;
        }

        public object executescaler(string commandText)
        {
            object obj = null;
            this.OpenCon();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = commandText;
            cmd.CommandTimeout = 600;
            obj = cmd.ExecuteScalar();
            this.CloseCon();
            this.Dispose();
            return obj;
        }

        public int executenonquery(string commandText)
        {
            int obj;
            this.OpenCon();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = commandText;
            cmd.CommandTimeout = 600;
            obj = cmd.ExecuteNonQuery();
            this.CloseCon();
            this.Dispose();
            return obj;
        }

        public DataSet ExecuteDataset(string commandTxt)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            this.OpenCon();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = commandTxt;
            cmd.CommandTimeout = 600;
            da.SelectCommand = cmd;
            da.Fill(ds);
            this.CloseCon();
            return ds;
        }

        public DataTable ExecuteDataTable(string commandTxt)
        {
            DataTable dt = new DataTable();
            this.OpenCon();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = commandTxt;
            cmd.CommandTimeout = 600;
            SqlDataReader dr = cmd.ExecuteReader();
            dt.Load(dr);
            this.CloseCon();
            return dt;
        }
        #endregion
    }
}
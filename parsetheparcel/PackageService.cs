﻿using System;
using System.Data;

namespace parsetheparcel
{
    public class PackageService : IPackageService
    {
        /// <summary>
        /// Parce the Package (Get Package Type and Cost)
        /// </summary>
        /// <param name="wsclient">WebService Client GUID (Hardcoded for now but could be implemented for basic security)</param>
        /// <param name="length">Parcel Length</param>
        /// <param name="breadth">Parcel Breadth</param>
        /// <param name="height">Parcel Height</param>
        /// <param name="weight">Parcel Weight</param>
        /// <param name="unit">Parcel Unit(mm/cm/m)</param>
        /// <param name="isTest">False/True(Not implemented but can be)</param>
        /// <returns>Response row (DataTable - used as it could return multiple)</returns>
        public DataTable ParcePackage(string wsclient, float length, float breadth, float height, float weight, string unit, bool isTest)
        {
            var dut = new DataUtility();
            var dt = new DataTable();

            try
            {
                dut.AddParameter("@wsclient", wsclient);
                dut.AddParameter("@plength", length);
                dut.AddParameter("@pbreadth", breadth);
                dut.AddParameter("@pheight", height);
                dut.AddParameter("@pweight", weight);
                dut.AddParameter("@punit", unit);
                dut.AddParameter("@test", isTest);
                dt = dut.ExecuteDataTable("testing.dbo.sp_GetPackageInfo");
                dt.TableName = "dtParcePackage";
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
    }
}
